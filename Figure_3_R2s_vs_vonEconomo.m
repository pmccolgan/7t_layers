% %04/12/18 P McColgan 
clear all 
% close all
signif_flag = 0;

% % Load data
load('../source_data/figure_5/R2s_VE_av')
load('../source_data/figure_5/R2s_VE_layer')
load('../source_data/figure_5/tot_cellcount')
load('../source_data/figure_5/cellcount')

% Total cell count vs. average R2* per ROI
% figure('DefaultAxesFontSize',18)
% %[R_VE,P_VE] = corr([tot_cellcount R2s_VE_av],'rows','pairwise');
% [R_VE,P_VE,RLO_VE,RUP_VE]= corrcoef(tot_cellcount, R2s_VE_av, 'alpha', 0.05,'rows','pairwise');
% plot(tot_cellcount,R2s_VE_av, 'o', 'MarkerFaceColor', 'b','MarkerSize',10);
% h = lsline;
% h.LineWidth = 4;
% set(h(1),'color','r')
% xlabel('VE cell count','FontSize',18,'FontWeight','bold')
% ylabel('Av. R2*','FontSize',18,'FontWeight','bold')

% 95% confidence intervals
% VE_LU_CI = [RLO_VE(2,1) RUP_VE(2,1)];

% Total cell count vs. R2* per layer per ROI
[R_R2s,P_R2s] = corr([R2s_VE_layer cellcount'],'rows','pairwise');
Rmax_R2s = R_R2s(1:8,9:14);
Pmax_R2s = P_R2s(1:8,9:14);       
 if signif_flag
        [r,c] = find((Pmax_R2s>(0.05/(8*6))));
        for x = 1:length(r)
                Rmax_R2s(r(x),c(x)) = 0;
        end 
 end
figure('DefaultAxesFontSize',20)
imR2s_VE = imagesc(Rmax_R2s); % cell content
R2s_layer_tot_cellcount = Rmax_R2s;
xticks(1:6)
title('R2* and VE cell counts','FontSize',24,'FontWeight','bold')
xlabel('VE layer','FontSize',20,'FontWeight','bold')
ylabel('R2* cortical depth (WM/GM <- Pial)','FontSize',20,'FontWeight','bold')
xticklabels({'I','II','III','IV','V','VI'})
yticks(1:8)
yticklabels({'D1','D2','D3','D4','D5','D6','D7','D8'})
%colormap inferno
colorbar
caxis([-.8 .8])
print('./figures/5C','-dpng','-r600');
