%plot layers
clear all 
close all

% Load 7T data
load('./figure_3/R1_ROI_vis')
load('./figure_3/R1_ROI_mot')
load('./figure_3/R1_ROI_aud')
load('./figure_3/R1_ROI_SP')


depth_idx = 8;
ROI_vis = {'V1' 'V2' 'V3' 'V4' 'V6' 'MT'};
ROI_mot = {'1' '2' '3A' '4' '3B'};
ROI_aud = {'A1' 'RI' 'PBelt' 'MBelt' 'LBelt'};
ROI_SP = {'7PL' 'LIPv' 'IP1' 'MIP' 'LIPd' };

% Plot visual ROIs
figure('DefaultAxesFontSize',18)
for i = 1:6
x = 1:depth_idx;
y = mean(R1_ROI_vis(:,i,:),3);
neg = reshape((std(R1_ROI_vis(:,i,:),0,3))./sqrt(size(R1_ROI_vis,3)),8,1);
pos = reshape((std(R1_ROI_vis(:,i,:),0,3))./sqrt(size(R1_ROI_vis,3)),8,1);
errorbar(x,y,neg,pos,'Linewidth',1.5)
hold on
end
legend(ROI_vis,'Location','northwest')
xlabel('Cortical depth ( Pial -> GM/WM boundary)')
ylabel('R1')


figure('DefaultAxesFontSize',18)
% % %Plot motor ROIs
for i = 1:5
x = 1:depth_idx;
y = mean(R1_ROI_mot(:,i,:),3);
neg = reshape((std(R1_ROI_mot(:,i,:),0,3))./sqrt(size(R1_ROI_mot,3)),8,1);
pos = reshape((std(R1_ROI_mot(:,i,:),0,3))./sqrt(size(R1_ROI_mot,3)),8,1);
errorbar(x,y,neg,pos,'Linewidth',1.5)
hold on
end 
legend(ROI_mot,'Location','northwest')
xlabel('Cortical depth ( Pial -> GM/WM boundary)')
ylabel('R1')


figure('DefaultAxesFontSize',18)
% % %Plot auditory ROIs
for i = 1:5
x = 1:depth_idx;
y = mean(R1_ROI_aud(:,i,:),3);
neg = reshape((std(R1_ROI_aud(:,i,:),0,3))./sqrt(size(R1_ROI_aud,3)),8,1);
pos = reshape((std(R1_ROI_aud(:,i,:),0,3))./sqrt(size(R1_ROI_aud,3)),8,1);
errorbar(x,y,neg,pos,'Linewidth',1.5)
hold on
end 
legend(ROI_aud,'Location','northwest')
xlabel('Cortical depth ( Pial -> GM/WM boundary)')
ylabel('R1')


figure('DefaultAxesFontSize',18)
% % %Plot superior parietal ROIs
for i = 1:5
x = 1:depth_idx;
y = mean(R1_ROI_SP(:,i,:),3);
neg = reshape((std(R1_ROI_SP(:,i,:),0,3))./sqrt(size(R1_ROI_SP,3)),8,1);
pos = reshape((std(R1_ROI_SP(:,i,:),0,3))./sqrt(size(R1_ROI_SP,3)),8,1);
errorbar(x,y,neg,pos,'Linewidth',1.5)
hold on
end 
legend(ROI_SP,'Location','northwest')
xlabel('Cortical depth ( Pial -> GM/WM boundary)')
ylabel('R1')

