
# Figure 1

average_R1_lh.0.5_layer.mgh - R1 sampled at 50% cortical depth (using https://github.com/kwagstyl/surface_tools)
average_R2s_lh.0.5_layer.mgh - R2s sampled at 50% cortical depth (using https://github.com/kwagstyl/surface_tools)
lh.inflated - Freesurfer average left inflated surface (this can loaded into freeview and the overlay option used to visualise average_R1_lh.0.5_layer.mgh and average_R2s_lh.0.5_layer.mgh.
 
# Figure 2 source data explanation

R1_ROI_vis - R1 values of Visual area, 3d matrix of layer x region x participants
R1_ROI_mot - R1 values of Motor area, 3d matrix of layer x region x participants
R1_ROI_aud - R1 values of Auditory area, 3d matrix of layer x region x participants
R1_ROI_SP -  R1 values of Superior parietal, area 3d matrix of layer x region x participants

R2s_ROI_vis - R2s values of Visual area, 3d matrix of layer x region x participants
R2s_ROI_mot - R2s values of Motor area, 3d matrix of layer x region x participants
R2s_ROI_aud - R2s values of Auditory area, 3d matrix of layer x region x participants
R2s_ROI_SP -  R2s values of Superior parietal, area 3d matrix of layer x region x participants

Use Figure_2_plots_R1.m and Figure_2_plots_R2s.m to recreate figure 2

# Figure 3 source data explanation

R1_VE_av - R1 averaged across 8 cortical depths for 43 regions of the Scholtens et al. 2018 MRI atlas  
R1_VE_layer - R1 for 8 cortical depths across 43 regions of the Scholtens et al. 2018 MRI atlas
R2s_VE_av - R2s averaged across 8 cortical depths for 43 regions of the Scholtens et al. 2018 MRI atlas  
R2s_VE_layer - R2s for 8 cortical depths across 43 regions of the Scholtens et al. 2018 MRI atlas
tot_cellcount - von Economo cell size for 43 regions of the Scholtens et al. 2018 MRI atlas 
cellcount - von Economo cell count for 6 layers across 43 regions of the Scholtens et al. 2018 MRI atlas 

R1_BB_av - R1 averaged across 8 cortical depths for 180 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas
R1_BB_layer - R1 for 8 cortical depths across 180 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas
R2s_BB_av - R2s averaged across 8 cortical depths for 180 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas
R2s_BB_layer - R2s for 8 cortical depths across 180 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas
HCP_BB_intensities_6L_av -  Big brain cell staining intensity for 180 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas
HCP_BB_intensities_6L - Big brain cell staining intensity for 6 layers across 180 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas

Use Figure_3_R2s_vs_vonEconomo.m and Figure_3_R1_vs_vonEconomo.m to recreate figure 3

# Figure 4 source data explanation

target_gene - list of layer specific genes from Bernard et al. 2012
target_gene_name_typ_2 - layer 2 specific genes
target_gene_name_typ_3 - layer 3 specific genes
target_gene_name_typ_4 - layer 4 specific genes
target_gene_name_typ_5 - layer 5 specific genes
target_gene_name_typ_6 - layer 6 specific genes
gene_name - list of genes from the Allen Human Brain Atlas following preprocessing as detailed in Arnatkevic̆iūtė et al. 2019
gene_data_nonan_zscore - z-scored genetic data with NaNs removed
cand_gene - list of candidate genes
R1roi - R1 for 177 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas (averaged across participants)
R2sroi - R2s for 177 regions of the HCP-MMP 1.0 Glasser et al. 2018 MRI atlas (averaged across participants)

Use Figure_4_layer_gene_graph.m, Figure_4_cand_gene_graph.m to recreate figure 4 and Figure_4_cand_gene_table.m to create supplemental_table_1

# Figure 5 source data explanation

Data is arranged in folders A, B, C, D. Each folder to contrains white matter connections subtypes (cortical-striatal, cortico-thalamic, cortico-cortical, inter-hemispheric and intra-hemispheric) for 34 regions of the Desikan-Killany atlas. 

Folders also include R1 or R2s for 8 cortical depths across 34 regions of the Desikan-Killany atlas (these values are averaged across 10 participants).
A - R1_av and white matter connections (streamline weighted)
B - R2s_av and white matter (streamline weighted)
C - R1_av and white matter connections (R1 weighted) 
D - R2s_av and white matter connections (R2s weighted) 

Use Figure_5a.m, Figure_5b.m, Figure_5c.m and Figure_5d.m to recreate figure 5