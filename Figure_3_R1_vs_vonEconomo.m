% %04/12/18 P McColgan 
clear all 
% close all
signif_flag = 0;

%load data
load('../source_data/figure_S2/R1_VE_av')
load('../source_data/figure_S2/R1_VE_layer')
load('../source_data/figure_S2/tot_cellcount')
load('../source_data/figure_S2/cellcount')

% Total cell count vs. average R1 per ROI
figure('DefaultAxesFontSize',18)
%[R_VE,P_VE] = corr([tot_cellcount R1_VE_av],'rows','pairwise');
[R_VE,P_VE,RLO_VE,RUP_VE] = corrcoef(tot_cellcount, R1_VE_av, 'alpha', 0.05);
plot(tot_cellcount,R1_VE_av, 'o', 'MarkerFaceColor', 'b','MarkerSize',10);
h = lsline;
h.LineWidth = 4;
set(h(1),'color','r')
xlabel('VE cell count','FontSize',18,'FontWeight','bold')
ylabel('Av. R1','FontSize',18,'FontWeight','bold')
print('./figures/S2_R1_A_BB','-dpng','-r600');

%cell count vs. average R1 per ROI per layer   
[R_R1,P_R1] = corr([R1_VE_layer cellcount'],'rows','pairwise');
Rmax_R1 = R_R1(1:8,9:14);
Pmax_R1 = P_R1(1:8,9:14);   
 if signif_flag
        [r,c] = find((Pmax_R1>(0.05/(8*6))));
        for x = 1:length(r)
                Rmax_R1(r(x),c(x)) = 0;
        end 
 end
figure('DefaultAxesFontSize',20)
imR1_VE = imagesc(Rmax_R1); % cell content
R1_layer_tot_cellcount = Rmax_R1;
xticks(1:6)
title('R1 and VE cell counts','FontSize',24,'FontWeight','bold')
xlabel('VE layer','FontSize',20,'FontWeight','bold')
ylabel('R1 cortical depth (WM/GM <- Pial)','FontSize',20,'FontWeight','bold')
xticklabels({'I','II','III','IV','V','VI'})
yticks(1:8)
yticklabels({'D1','D2','D3','D4','D5','D6','D7','D8'})
colorbar
caxis([-.8 .8])
print('./figures/S2_R1_B_VE','-dpng','-r600');
