%connectome vs 7T layer postprocessing
%01.07.19
clear all
% close all

signif_flag = 0;

%Load data
load('../source_data/figure_7/C/stri_cort_av')
load('../source_data/figure_7/C/thal_cort_av')
load('../source_data/figure_7/C/cort_cort_av')
load('../source_data/figure_7/C/inter_av')
load('../source_data/figure_7/C/intra_av')
load('../source_data/figure_7/C/R1_av')
m.R1_av = R1_av; 

% Load 7T data
metrics = {'R1'};
figure('DefaultAxesFontSize',20)
for i = 1:length(metrics)    
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];
    [Ri,Pi] = corr([stri_cort_av thal_cort_av cort_cort_av inter_av intra_av m.(str3)' ],'rows','pairwise');
    Rimax = Ri(6:13,1:5);
    Pimax = Pi(6:13,1:5);
    if signif_flag

        [r,c] = find((Pimax>(0.05/(8*5))));
        for x = 1:length(r)
                 Rimax(r(x),c(x)) = 0;
        end 
    end
    imagesc(Rimax)
    title('R1-weighted connectivity','FontSize',24,'FontWeight','bold')
    xlabel('White matter connection types','FontSize',20,'FontWeight','bold')
    ylabel('R1 cortical depth (WM/GM <- Pial)','FontSize',20,'FontWeight','bold')
    xticks(1:5)
    xticklabels({'C-S','C-T','C-C','Inter-H','Intra-H'})
    yticks(1:8)
    yticklabels({'D1','D2','D3','D4','D5','D6','D7','D8'})
    colorbar
    caxis([-.8 .8])
    print('./figures/7C','-dpng','-r600');
end  
