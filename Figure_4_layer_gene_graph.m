% Script for PCA gene analysis
% P McColgan 07.12.18
% MPI-CBS
clear all
signif_flag = 1;

%Load data
load('./figure_6/target_gene')
load('./figure_6/gene_name')
load('./figure_6/gene_data_nonan_zscore')
load('./figure_6/R2s_av')
load('./figure_6/target_gene_name_type_2')
load('./figure_6/target_gene_name_type_3')
load('./figure_6/target_gene_name_type_4')
load('./figure_6/target_gene_name_type_5')
load('./figure_6/target_gene_name_type_6')

m.source_gene_name = gene_name;
m.source_gene_data_nonan_zscore = gene_data_nonan_zscore;
m.target_gene_name_type_2 = target_gene_name_type_2;
m.target_gene_name_type_3 = target_gene_name_type_3;
m.target_gene_name_type_4 = target_gene_name_type_4;
m.target_gene_name_type_5 = target_gene_name_type_5;
m.target_gene_name_type_6 = target_gene_name_type_6;
m.R2s_av = R2s_av;

% Match data type 2 (lamina 2 genes)
m.match_idx_type_2 = find(ismember(m.source_gene_name,m.target_gene_name_type_2)); % index refers to Allen data index
m.match_data_type_2 = m.source_gene_data_nonan_zscore(:,m.match_idx_type_2); 

% Match data type 3 (lamina 3 genes)
m.match_idx_type_3 = find(ismember(m.source_gene_name,m.target_gene_name_type_3)); % index refers to Allen data index
m.match_data_type_3 = m.source_gene_data_nonan_zscore(:,m.match_idx_type_3);

% Match data type 4 (lamina 4 genes)
m.match_idx_type_4 = find(ismember(m.source_gene_name,m.target_gene_name_type_4)); % index refers to Allen data index
m.match_data_type_4 = m.source_gene_data_nonan_zscore(:,m.match_idx_type_4); 

% Match data type 5 (lamina 5 genes)
m.match_idx_type_5 = find(ismember(m.source_gene_name,m.target_gene_name_type_5)); % index refers to Allen data index
m.match_data_type_5 = m.source_gene_data_nonan_zscore(:,m.match_idx_type_5); 

% Match data type 6 (lamina 6 genes)
m.match_idx_type_6 = find(ismember(m.source_gene_name,m.target_gene_name_type_6)); % index refers to Allen data index
m.match_data_type_6 = m.source_gene_data_nonan_zscore(:,m.match_idx_type_6);
 
% % PCA on gene lists
[~,sl2] = pca(m.match_data_type_2);
[~,sl3] = pca(m.match_data_type_3);
[~,sl4] = pca(m.match_data_type_4);
[~,sl5] = pca(m.match_data_type_5);
[~,sl6] = pca(m.match_data_type_6);

figure('DefaultAxesFontSize',16)
%Create MPM average values 
metrics = {'R2s'};
for i = 1:length(metrics) 
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];   
    %layers 2
    [rho_L2.(str3),pval_L2.(str3),RLO_L2.(str3),RUP_L2.(str3)] = corrcoef(sl2(:,1), mean(m.(str3),1)', 'alpha', 0.05,'rows','pairwise');
%     plot(sl2(:,1),mean(m.(str3)), 'o', 'MarkerFaceColor', 'b','MarkerSize',10)
%     h = lsline;
%     h.LineWidth = 4;
%    set(h(1),'color','r')
%     print(['./Figure_6/L2_' str0],'-depsc','-r600');
%     figure('DefaultAxesFontSize',16)
end

%Create MPM average values 
metrics = {'R2s'};
for i = 1:length(metrics) 
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];   
    %layers 3
    [rho_L3.(str3),pval_L3.(str3),RLO_L3.(str3),RUP_L3.(str3)] = corrcoef(sl3(:,1), mean(m.(str3),1)', 'alpha', 0.05,'rows','pairwise');
%     plot(sl3(:,1),mean(m.(str3)), 'o', 'MarkerFaceColor', 'b','MarkerSize',10)
%     h = lsline;
%     h.LineWidth = 4;
%     set(h(1),'color','r')
%     print(['./Figure_6/L3_' str0],'-depsc','-r600');
%     figure('DefaultAxesFontSize',16)
end

%Create MPM average values 
metrics = {'R2s'};
for i = 1:length(metrics) 
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];   
    %layers 4
    [rho_L4.(str3),pval_L4.(str3),RLO_L4.(str3),RUP_L4.(str3)] = corrcoef(sl4(:,1), mean(m.(str3),1)', 'alpha', 0.05,'rows','pairwise');
%     plot(sl4(:,1),mean(m.(str3)), 'o', 'MarkerFaceColor', 'b','MarkerSize',10)
%     h = lsline;
%     h.LineWidth = 4;
%     set(h(1),'color','r')
%     print(['./Figure_6/L4_' str0],'-depsc','-r600');
%     figure('DefaultAxesFontSize',16)
end

%Create MPM average values 
metrics = {'R2s'};
for i = 1:length(metrics) 
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];
    %layers 5
    [rho_L5.(str3),pval_L5.(str3),RLO_L5.(str3),RUP_L5.(str3)] = corrcoef(sl5(:,1), mean(m.(str3),1)', 'alpha', 0.05,'rows','pairwise');
%     plot(sl5(:,1),mean(m.(str3)), 'o', 'MarkerFaceColor', 'b','MarkerSize',10)
%     h = lsline;
%     h.LineWidth = 4;
%     set(h(1),'color','r')
%     print(['./Figure_6//L5_' str0],'-depsc','-r600');
%     figure('DefaultAxesFontSize',16)
end

%Create MPM average values 
metrics = {'R2s'};
for i = 1:length(metrics) 
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];   
    %layers 6
    [rho_L6.(str3),pval_L6.(str3),RLO_L6.(str3),RUP_L6.(str3)] = corrcoef(sl6(:,1), mean(m.(str3),1)', 'alpha', 0.05,'rows','pairwise');
%     plot(sl6(:,1),mean(m.(str3)), 'o', 'MarkerFaceColor', 'b','MarkerSize',10)
%     h = lsline;
%     h.LineWidth = 4;
%     set(h(1),'color','r')
%     figure('DefaultAxesFontSize',16)
end

%95% 

for i = 1:length(metrics)    
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];
    [Ri,Pi] = corr([sl2(:,1) sl3(:,1) sl4(:,1) sl5(:,1) sl6(:,1) m.(str3)' ],'rows','pairwise');
    
        if signif_flag   
        [r,c] = find((Pi>(0.05/(8*5))));
        for x = 1:length(r)
                 Ri(r(x),c(x)) = 0;
        end 
        end
    
    Rimax = Ri(6:13,1:5);
    Pimax = Pi(6:13,1:5);        
    imagesc(Rimax)
    xticks([1 2 3 4 5])
    xticklabels({'L2','L3','L4','L5','L6'})
    yticks([1 2 3 4 5 6 7 8])
    yticklabels({'D1','D2','D3','D4','D5','D6','D7','D8'})
    colormap jet
    colorbar
    caxis([-0.8 0.8])

end  

%95% confidence intervals
Layer_LL_CI = [ RLO_L2.R2s_av(2,1) ; RLO_L3.R2s_av(2,1) ; RLO_L4.R2s_av(2,1) ; RLO_L5.R2s_av(2,1) ; RLO_L6.R2s_av(2,1)];
Layer_UL_CI = [ RUP_L2.R2s_av(2,1) ; RUP_L3.R2s_av(2,1) ; RUP_L4.R2s_av(2,1) ; RUP_L5.R2s_av(2,1) ; RUP_L6.R2s_av(2,1)];
Layer_CI = cat(2,Layer_LL_CI,Layer_UL_CI);
