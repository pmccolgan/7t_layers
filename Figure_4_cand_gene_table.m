% Candidiate Gene analysis
clear all
close all
subj = 29;
ROI = 180;


%Load data
load('./figure_6/gene_name')
load('./figure_6/cand_gene')
load('./figure_6/gene_data_nonan_zscore')
load('./figure_6/R2sroi')
load('./figure_6/R1roi')
m.source_gene_data_nonan_zscore = gene_data_nonan_zscore;
m.source_gene_name = gene_name;

%Candidate gene correlation
%cand_gene = {'GRIN2B' 'SNCA' 'APOE' 'MAPT' 'RRM2B' 'VAMP1' 'KCNA1' 'ATP2B2' 'ADAM23'};
cand_gene = {'HTT' 'FAN1' 'RRM2B' 'PMS2' 'MLH1' 'RASGRP1' 'OLFM1' 'TPPP' 'CHGB' 'GABRD' 'ITGB4' 'KCNA1' 'NEFM' 'ATP1A1' 'ATP2A2' 'ATP2B2' 'PAK1' 'GLRX2' 'NRN1' 'IL17RB' 'PRMT8' 'NRIP3' 'SNAP25' 'VAMP1' 'UXS1' 'TSPYL5' 'ADAM23' 'DCLK1' 'DLGAP1'};
for i = 1:length(cand_gene)
    str0 = [cand_gene{i}];
    str1 = ['R1_' str0];
    str2 = ['R1_' str0];
    str3 = ['R2s_' str0];
    str4 = ['R2s_' str0];
cand_gene_data = m.source_gene_data_nonan_zscore(:,(find(ismember(m.source_gene_name,(str0)))));
%%%%%%%%%%%%%% R2* %%%%%%%%%%%%%% 
[rho_R2,pval_R2] = corr([cand_gene_data R2sroi'],'rows','pairwise');
rho_tot_R2(i) = rho_R2(2,1);
pval_tot_R2(i) = pval_R2(2,1);
%%%%%%%%%%%% R1 %%%%%%%%%%%%%%%%%%%%
[rho_R1,pval_R1] = corr([cand_gene_data R1roi'],'rows','pairwise');
rho_tot_R1(i) = rho_R1(2,1);
pval_tot_R1(i) = pval_R1(2,1);
end
%%%%%%%%%% FDR correction %%%%%%%%%%%%
R2_pval_fdr = mafdr(pval_tot_R2,'BHFDR','true');
R1_pval_fdr = mafdr(pval_tot_R1,'BHFDR','true');
%%%%%%%%%%%%% Create table %%%%%%%%%%%%%%%%%%
data = table;
data.names = cand_gene'; 
data.R2_rho = rho_tot_R2';
data.R2_pval = pval_tot_R2';
data.R2_pval_fdr = R2_pval_fdr';
data.R1_rho = rho_tot_R1';
data.R1_pval = pval_tot_R1';
data.R1_pval_fdr = R1_pval_fdr';
% write table
%writetable(data,'supplemental_table_1.xlsx')
