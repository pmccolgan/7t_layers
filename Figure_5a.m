% connectome vs 7T layer postprocessing
% 01.07.19
clear all
% close all
signif_flag = 0;

%Load data
load('../source_data/figure_7/A/stri_cort_av')
load('../source_data/figure_7/A/thal_cort_av')
load('../source_data/figure_7/A/cort_cort_av')
load('../source_data/figure_7/A/inter_av')
load('../source_data/figure_7/A/intra_av')
load('../source_data/figure_7/A/R1_av')
m.R1_av = R1_av; 

m.nodes_rmv = [35 39 41 42 46 48 49 84]; %remove cerebellum and amygdala, GP, NA
% process only left regions
stri = [35 36 37 39 40 41];
l_thal = 35;
l_stri = [36 37];
l_hemi = 1:34;
r_hemi = 43:76;

% Load 7T data
metrics = {'R1'};
figure('DefaultAxesFontSize',20)
for i = 1:length(metrics)
    str0 = [metrics{i}];
    str3 = [metrics{i} '_av'];
    [Ri,Pi] = corr([stri_cort_av thal_cort_av cort_cort_av inter_av intra_av m.(str3)' ],'rows','pairwise');
    Rimax = Ri(6:13,1:5);
    Pimax = Pi(6:13,1:5);
    if signif_flag
        [r,c] = find((Pimax>(0.05/(8*5))));
        for x = 1:length(r)
            Rimax(r(x),c(x)) = 0;
        end
    end
    imagesc(Rimax)
    title('Streamline-weighted connectivity','FontSize',24,'FontWeight','bold')
    xlabel('White matter connection types','FontSize',20,'FontWeight','bold')
    ylabel('R1 cortical depth (WM/GM <- Pial)','FontSize',20,'FontWeight','bold')
    xticks(1:5)
    xticklabels({'C-S','C-T','C-C','Inter-H','Intra-H'})
    yticks(1:8)
    yticklabels({'D1','D2','D3','D4','D5','D6','D7','D8'})
    colorbar
    set(gca,'fontsize',18)
    colorbar
    caxis([-.8 .8])
    print('./figures/7A','-dpng','-r600');
end

